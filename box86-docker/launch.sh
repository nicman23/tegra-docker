#!/bin/bash
build() {
	docker build -t steam-box86 .	
}

run() {
#	x11docker -p --gpu \
#		--hostipc --hostdbus \
#		--init=systemd \
#		--network=host \
#		--no-setup \
#		--group-add video \
#		--group-add render \
#		steam-box86
	docker run --privileged --rm -it --network=host --runtime nvidia -v /tmp/.X11-unix/:/tmp/.X11-unix steam-box86 bash
}

$1
