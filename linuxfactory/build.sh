#!/bin/bash
set -e

export NO_COMPRESS=true \
	DOCKER_BUILDKIT=1 \
	COMPOSE_DOCKER_CLI_BUILD=1 \
  BUILDKIT_PROGRESS=plain \
	NAME="switchroot-${DISTRO}-$(date '+%Y-%m-%d')"

# Build image
docker build -f $PLATFORM/Dockerfile.$DISTRO -t l4t-$DISTRO $PLATFORM --platform linux/arm64

# Export image as tar
docker export -o "l4t-${DISTRO}.tar" `docker run -d l4t-$DISTRO /bin/true`

# Remove container afterwards
docker rmi -f `docker images -q l4t-$DISTRO`

# Create disk image
virt-make-fs l4t-${DISTRO}.tar "${NAME}.img" --size=+768M -t ${TYPE:-ext4} --label "SWR-$(echo ${DISTRO:0:3} | tr '[:lower:]' '[:upper:]')"

# Zerofree the image 
zerofree -n "${NAME}.img"

if [[ -n $BOOT || -n $HEKATE ]]; then
	git clone https://gitlab.com/switchroot/bootstack/bootstack-build-scripts
	./bootstack-build-scripts/build.sh .
fi

if [[ -n $HEKATE ]]; then
	# Get build directory size
	size="$(du -b -s "${NAME}.img" | awk '{print int($1);}')"

	# Alignement adjust to 4MB
	aligned_size=$(((${size} + (4194304-1)) & ~(4194304-1)))

	# Check if image needs alignement
	align_check=$((${aligned_size} - ${size}))

	# Align diskimage if necessary
	if [[ ${align_check} -ne 0 ]]; then
		dd if=/dev/zero bs=1 count=${align_check} >> "${NAME}.img"
	fi

	mkdir -p switchroot/install/ bootloader/

	# Split into 4GB part
	split -b4290772992 --numeric-suffixes=0 "${NAME}.img" switchroot/install/l4t.

	# Finally create 7zip
	7z a "${NAME}.7z" ./switchroot ./bootloader

	# Clean diskimage to avoid copying it in docker
	rm -rf "${NAME}.img"
fi
