# LinuxFactory

## Usage

Build the docker image : 

```
docker build -t linuxfactory .
```

Run the container and build `fedora` distribution for `icosa` platform :

```
docker run --rm -it --privileged -e DISTRO=fedora -e PLATFORM=icosa -v /var/run/docker.sock:/var/run/docker.sock -v $(pwd):/out linuxfactory
```

Or if you have qemu static and binftm you can run:
```
DISTRO=hirsute PLATFORM=icosa ./build.sh
```
